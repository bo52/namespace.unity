program namespace;

uses
  Vcl.Forms,
  main in 'main.pas' {fm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tfm, fm);
  Application.Run;
end.
